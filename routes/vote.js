// import lib
const express = require('express');
const db = require('../util/db.config');
// define variable
const sequelize = db.sequelize;
const { Framework } = db.vote;
// const { Post, PostDescription } = db.post;
const route = express.Router();

// include auth


// เส้นทางต่อไปนี้ !!ไม่ต้อง!! Authorize ผ่าน JWT ก่อน
route.get('/realtimevote/:id', async (req, res, next) => {
  const framework_id = req.params.id;
  let resultUpdate = []
  if (framework_id == "showinit") {
    resultUpdate = await plusVote(framework_id)
  } else {
    const addVote = await plusVote(framework_id)
    resultUpdate = await updater(framework_id,addVote)
  }


  // const resultUpdate = [300,50,100]
  res.json({ "name": "TEST", "val": resultUpdate }).end();
});
const updater = async (framework_id,addVote) => {
  updateVote = await sequelize.transaction(function (t) {
    return Framework.update(
      addVote,
      { where: { framework_id: framework_id } },
      { transaction: t }
    );
  });
  if (updateVote) {
    let newArr = []
      const result = await Framework.findAll();
      result.map((item, i) => {
        try {
          newArr = [...newArr, item.framework_vote]
        } catch (error) {
          console.error(error);

        }
      })
      return newArr
  } else {
    console.log('ERROR');
  }
}

const plusVote = async (framework_id) => {
  try {
    if (framework_id == "showinit") {
      let newArr = []
      const result = await Framework.findAll();
      result.map((item, i) => {
        try {
          newArr = [...newArr, item.framework_vote]
        } catch (error) {
          console.error(error);

        }
      })
      return newArr
    } else {
      let newArr = {}
      const result = await Framework.findAll({
        where: {
          framework_id: framework_id
        }
      });
      newArr['framework_vote'] = result[0].framework_vote+1
      return newArr
    }
  } catch (error) {
    console.error(error);
  }
}



module.exports = route;