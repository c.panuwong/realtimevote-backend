"use strict";
const dbsso = require('../util/dbsso.config');
// define variable
const { user, user_group } = dbsso.login;

class Usermanage {

    constructor() {
    }

    finduserbyid(user_id) {


        return new Promise(async function (res, rej) {
           
            const userrs = await user.findAll({
                where: {
                  user_id: user_id,
                },
                include: [
                  {
                    model: user_group, as: 'user_group',
                    required: false,
                    where: {
                      user_group_status: 1,
                    }
                  }
                ]
              });
              res(userrs)
        })

    }

    findlikealready(wholike,user_id) {


        return new Promise(async function (res, rej) {
           
            const rs = await wholike.map(async function (item, i) {
                if (item.user_id == user_id) {
                   return item.id
                }
            })
            res(rs)
        })

    }




}


module.exports = Usermanage;