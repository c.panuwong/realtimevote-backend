module.exports = (sequelize, Sequelize) => {
  const Framework = sequelize.define(
    'framework',
    {
      framework_id: {
        type: Sequelize.INTEGER,
        field: 'framework_id',
        primaryKey: true
      },
      framework_name: {
        type: Sequelize.STRING,
        field: 'framework_name'
      },
      framework_vote: {
        type: Sequelize.INTEGER,
        field: 'framework_vote'
      }
    },
    {
      timestamps: false,
      freezeTableName: true
    }
  );

  

  return { Framework };
};