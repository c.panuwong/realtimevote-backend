// import express & define port = 3000
const express = require('express');
const app = express();
const port = 8082;
const bodyParser = require('body-parser');
require('./util/mongo.config');
var publicDir = require('path').join(__dirname,'/assets');
app.use(express.static(publicDir));


// set use body json
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({ extended: false })); // support encoded bodies

// Add headers
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,authorization');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});



// add route
const voteRoute = require('./routes/vote');
app.use('/vote', voteRoute);


// set port & run server
const server = app.listen(port, () => console.log(`listening on port ${port}`));
const io = require('socket.io')(server);

// รอการ connect จาก client
io.on('connection', client => {
    console.log('user connected')
  
    // เมื่อ Client ตัดการเชื่อมต่อ
    client.on('disconnect', () => {
        console.log('user disconnected')
    })

    // ส่งข้อมูลไปยัง Client ทุกตัวที่เขื่อมต่อแบบ Realtime
    client.on('addVote', function (message) {
        console.log(message +" & server Response realtime");
        io.sockets.emit('newVal', message)
    })
})
